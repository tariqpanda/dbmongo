<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>@yield('title', 'Kraeken')</title>

        <!-- Styles -->
        <link rel="stylesheet" type="text/css" href="{{ asset('css/bulma.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ asset('css/bulma-helpers.css') }}">
        @stack('styles')

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

    </head>
    <body class="@yield('bodyclass')">
    <a href="/"><img src="https://islandvideo.ca/islandapp/wp-content/uploads/2018/02/f4a2128f7fa9ba69b4a67dfb48539b17_partydjadelaideicon-4.png" alt></a>
    <nav class="navbar is-info" role="navigation" aria-label="main navigation">
        <div class="navbar-burger burger" data-target="navMenu">
            <span></span>
            <span></span>
            <span></span>
        </div>
        <div id="navMenu" class="navbar-menu">
            <div class="navbar-start">
                <a href="/" class="navbar-item">
                    Home
                </a>

                <a href="/zenders" class="navbar-item">
                    Zenders
                </a>

                <a href="/contact" class="navbar-item">
                    Contact
                </a>
                @can('manage-users')
                <a class="navbar-item" href="{{ route('admin.users.index') }}"> User Manangement</a>
                @endcan
                @guest
                    <a href="/login" class="navbar-item">
                        Login
                    </a>
                @else
                    <a class="navbar-item" href="{{ route('logout') }}"
                       onclick="event.preventDefault();document.getElementById('logout-form').submit();">
                        {{ __('Logout') }}
                    </a>

                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>
                @endguest
                </div>
            </div>
    </nav>

    <div class="content container is-fullhd">
            <div class="container is-fluid">
                @if(Session::has('success'))
                    <div class="notification is-success has-margin-30">
                        <button class="delete"></button>
                        <div class="alert">
                            {{ Session::get('success') }}
                        </div>
                    </div>
                @endif
                @if(Session::has('deleted'))
                    <div class="notification is-danger has-margin-30">
                        <button class="delete"></button>
                        <div class="alert">
                            {{ Session::get('deleted') }}
                        </div>
                    </div>
                @endif
                @if ($errors->any())
                    @foreach ($errors->all() as $error)
                        <div class="notification is-danger has-margin-30">
                            <button class="delete"></button>
                            <div class="alert">
                                {{ ucfirst($error) }}
                            </div>
                        </div>
                    @endforeach
                @endif
                    <div class="columns">
                        <div class="column">
                            <h1 id="h1" class="has-padding-top-15" style="text-align: center;">@yield('h1')</h1>
                        </div>
                    </div>
                    @yield('content')
            </div>
        </div>
        <script src="{{ asset('js/kraeken.js') }}"></script>
        <script src="{{ asset('js/app.js') }}"></script>
    </body>
