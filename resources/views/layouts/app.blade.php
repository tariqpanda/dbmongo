<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/kraeken.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/bulma.css') }}" rel="stylesheet">
</head>
<body>
{{--<div class="container is-fullhd">
    <nav class="navbar" role="navigation" aria-label="main navigation">
        <div class="navbar-burger burger" data-target="navMenu">
            <span></span>
            <span></span>
            <span></span>
        </div>
        <div id="navMenu" class="navbar-menu">
        <a class="navbar-item" href="{{ route('admin.users.index') }}"> User Manangement</a>
    </div>
    </nav>
</div>--}}

<main class="py-4">
    @yield('content')
</main>
</body>
</html>
