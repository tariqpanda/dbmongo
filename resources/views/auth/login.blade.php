@extends('layouts.layout')


@section('content')

    <section class="hero is-full-height">
        <div class="hero-body">
            <div class="container">
                <div class="columns is-centered">
                    <div class="column is-3-tablet-only is-6-desktop is-4-widescreen">
                        <form class="box" method="POST" action="{{ route('login') }}">
                            @csrf

                            <div class="field">
                                <label class="label" for="email">{{ __('E-Mail') }}</label>
                                <div>
                                    <input id="email" type="email" class="control input @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                                    @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="field">
                                <label class="label" for="password">{{ __('Wachtwoord') }}</label>

                                <div>
                                    <input id="password" type="password" class="control input @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">

                                    @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="field">
                                    <div class="form-check">
                                        <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                                        <label class="checkbox" for="remember">
                                            {{ __('Remember Me') }}
                                        </label>
                                    </div>
                            </div>
                            <div class="field">
                                    <button type="submit" class="button is-primary">
                                        {{ __('Login') }}
                                    </button>
                                </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
