@extends('layouts.app')

@section('content')

    <div class="hero is-full-height">
      <div class="hero-body">
        <div class="container is-fluid">
            <div class="columns is-centered">
                <div class="column is-3-tablet-only is-6-desktop is-3-widescreen">
                    <form class="box" method="POST" action="{{ route('register') }}">
                        @csrf

                        <div class="field">
                            <label class="label" for="name">{{ __('Naam') }}</label>
                            <div>
                                <input id="name" type="text" class="control input @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>

                                @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="field">
                            <label class="label" for="email">{{ __('E-Mail') }}</label>
                            <div>
                                <input id="email" type="email" class="control input @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="field">
                            <label class="label" for="password">{{ __('Wachtwoord') }}</label>

                            <div>
                                <input id="password" type="password" class="control input @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="field">
                            <label class="label" for="password-confirm">{{ __('Bevestig Wachtwoord') }}</label>

                            <div>
                                <input id="password-confirm" type="password" class="control input" name="password_confirmation" required autocomplete="new-password">
                            </div>
                        </div>

                        <div class="field">
                                <button type="submit" class="button is-primary">
                                    {{ __('Register') }}
                                </button>
                            </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
