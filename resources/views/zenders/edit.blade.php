@extends('layouts.layout')
@section('title', $zenders->title)
@section('h1', 'De zender '.$zenders->naam.' aanpassen')
@section('content')
    <form method="post" action="{{ route('zenders.update', $zenders->slug) }}">

        <div class="columns">
            @csrf
            @method('PATCH')
            <div class="column">
                <label for="name"><h2>Naam</h2></label>
                <input type="text" class="input" name="naam" value="{{ $zenders->naam }}">
            </div>
        </div>

        <div class="columns">
            <div class="column">
                <label for="name"><h2>Omschrijving</h2></label>
                <textarea id="editor" class="input" style="height: 190px;" name="inhoud">{{ $zenders->body }} </textarea>
            </div>
        </div>

        <div class="columns">
            <div class="column">
                <button type="submit" class="button is-success">Opslaan</button>
            </div>
        </div>
    </form>
@endsection
