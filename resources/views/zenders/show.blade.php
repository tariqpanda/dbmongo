@extends ('layouts.layout')
@section('title',$zenders->naam)


@section ('content')
    <div class="content">
            @guest
                <div class="column is-one-third">
                    <div class="card">
                        <div class="card-content">
                            <h2 class="lead"> {!! $zenders->naam !!}</h2>
                            <h2 class="lead"> {!! $zenders->body!!}</h2>
                        </div>
                    </div>
                </div>

            @else

                <div class="column is-one-third">
                    <div class="card">
                        <div class="card-content">
                                <h2 class="lead"> {!! $zenders->naam !!}</h2>
                                <h2 class="lead"> {!! $zenders->body!!}</h2>
                            </div>
                            <footer class="card-footer">
                                <a href="{{ route('zenders.edit', $zenders->slug) }}"
                                   class="card-footer-item button is-warning">Aanpassen</a>
                                <a class="card-footer-item button is-danger"
                                   onclick="checkBeforeSubmit()">Verwijderen</a>
                                <script>
                                    function checkBeforeSubmit() {
                                        if (!confirm('Weet je zeker dat je dit wil verwijderen?')) return;
                                        document.getElementById('delete-form').submit();
                                    }
                                </script>

                                <form id="delete-form" action="{{ route('zenders.destroy', $zenders->slug) }}" method="POST"
                                      style="display: none;">
                                    @csrf
                                    @method('DELETE')
                                </form>
                            </footer>
                        </div>
                    </div>
                    @endguest
                </div>
@endsection

