@extends ('layouts.layout')

@section('title', 'Zenders')
@section('bodyclass','index')

@section('h1', 'Zenders')

@section('content')
    @guest
        <div class="columns">
            <div class="column has-text-centered">
                <a href="/contact" class="button is-rounded is-success is-outlined">Neem contact op</a>
            </div>
        </div>
    @else
        <div class="columns">
            <div class="column has-text-centered">
                <a href="{{route('zenders.create')}}" class="button is-rounded is-success">Maak een nieuwe zender aan</a>
            </div>
        </div>
    @endguest
    @guest
        @else
    <form action="{{route('zenders.search')}}" method="get">
        <div class="columns has-text-centered">
            <div class="column is-half">
                <input type="text" value="{{ app('request')->get('query') }}" name="query" class="input is-rounded"
                       placeholder="Programma/Zender zoeken">
            </div>
            <div class="column has-text-centered ">
                <button type="submit" class="button is-rounded is-info is-fullwidth is-outlined">Zoeken</button>
            </div>
        </div>
    </form>
    @endguest
    <div class="columns has-margin-top-50">
        @forelse($zenders as $key => $data)
            @if($loop->index >= 2 && $loop->index % 3 == 0)
    </div>
    <div class="columns">
        @endif
        <div class="column is-one-third">
            <div class="index">

                <div class="card">
                    <header class="card-header">
                        <p class="card-header-title">
                            {{ substr(strip_tags($data->naam), 0,60)}}{{ strlen(strip_tags($data->naam)) >  60 ? "..." : ""}}
                        </p>
                    </header>
                    <div class="card-content">
                        <div class="content">
                            {!! substr(strip_tags($data->body), 0,120)!!}{!! strlen(strip_tags($data->body)) >120 ? "..." : ""!!}
                        </div>
                      </div>
                    <footer class="card-footer">
                        <a href="{{route('zenders.show', $data->slug)}}" class="card-footer-item button is-info">Programmaoverzicht</a>
                        @guest
                        @else
                            <a class="card-footer-item button is-warning"
                               href="{{route('zenders.edit', $data->slug)}}">Edit</a>
                        @endguest
                    </footer>
                </div>
            </div>
         </div>



        @empty
            <th><h3>Geen zenders gevonden</h3></th>
        @endforelse
    </div>
    @if(!$zenders->isEmpty())
        {{ $zenders->appends(request()->input())->links() }}
    @endif
@stop
