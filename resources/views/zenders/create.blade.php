@extends ('layouts.layout')
@section('title','Zender toevoegen')
@section('h1','Zender toevoegen')
@section ('content')
    <form id="save_post" method="POST" action="{{route('zenders.store')}}">
        @csrf

        <div class="columns">
            <div class="column">
                <label for="name"><h2>Zender</h2></label>
                <input type="text" id="naam" class="input" name="naam" value="{{ old('naam') }}">
            </div>
        </div>

        <div class="columns">
            <div class="column">
                <label for="name"><h2>Omschrijving</h2></label>
                <textarea id="editor" class="input" style="height: 145px;" name="inhoud">{{ old('inhoud') }}</textarea>
            </div>
        </div>


        <div class="columns">
            <div class="column">
                <input type="submit" class="button is-success" value="Opslaan">
            </div>
        </div>

    </form>
@endsection
