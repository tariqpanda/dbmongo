<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Zenders extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'naam', 'inhoud', 'slugs', 'id'
    ];
}
