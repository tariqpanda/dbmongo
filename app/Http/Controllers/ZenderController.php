<?php

namespace App\Http\Controllers;

use App\Programma;
use Illuminate\Http\Request;
use App\Zenders;
use Illuminate\Http\Response;
use Illuminate\Support\Str;
use Illuminate\Validation\ValidationException;


class ZenderController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth', ['only' => ['edit', 'create', 'store', 'destroy']]);

    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $zenders = Zenders::orderBy('naam')->paginate(env('APP_PAGINATION_LIMIT', 6));

        return view('zenders.index', compact('zenders'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {

        return view('zenders.create');
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return Response
     * @throws ValidationException
     */
    public function store(Request $request)
    {
        //Data Valideren
        $this->validate(
            $request, [
            'naam' => 'required|min:1|unique:zenders,naam,NULL,id,deleted_at,NULL',
            'inhoud' => 'required',
        ]);
        //Data opslaan in database
        $zenders = new Zenders;
        $zenders->naam = $request->naam;
        $zenders->body = $request->inhoud;
        $zenders->slug = Str::slug($request->naam, '-');


        //Data versuren naar een applicatie
        $zenders->save();

        return redirect()->route('zenders.show', $zenders->slug)->with('success', 'Zender aangemaakt');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        // Zet de slug variable als url
        $slug = strtolower($id);
        $zenders = Zenders::where('slug', '=', $slug)->get()->first();
        if (!$zenders) {
            abort(404);
        }

        return view('zenders.show', compact('zenders'));
    }


/*    public function search(Request $request)
    {
        $terms = new Collection();
        $query = $request->get('query');
        $category = $request->get('category_id');
        if ($query || $category) {
            $termsQuery = Terms::with('categories');
            if ($category) {
                $termsQuery->whereHas('categories', function ($q) use ($category) {
                    $q->where('categories_id', $category);
                });
            }
            if ($query) {
                $termsQuery->where('title', 'like', '%' . $query . '%');
            }

            $terms = $termsQuery->paginate(env('APP_PAGINATION_LIMIT', 5));

        }
        return view('terms.index', compact('terms'));
    }*/


    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        // Zet de slug variable als url
        $slug = strtolower($id);
        $zenders = Zenders::where('slug', '=', $slug)->get()->first();
        if (!$zenders) {
            abort(404);
        }
        return view('zenders.edit', compact('zenders'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param int $id
     * @return Response
     * @throws ValidationException
     */
    public
    function update(Request $request, $id)
    {
        //Data Valideren
        $this->validate(
            $request, [
            'naam' => 'required|min:1|unique:zenders,naam,NULL,id,deleted_at,NULL' . $id,
            'inhoud' => 'required',
        ]);

        $slug = strtolower($id);
        $zenders = Zenders::where('slug', '=', $slug)->get()->first();
        if (!$zenders) {
            abort(404);
        }

        //Data opslaan in database
        $zenders->naam = $request->naam;
        $zenders->body = $request->inhoud;
        $zenders->slug = Str::slug($request->naam, '-');

        //Data versuren naar een applicatie
        $zenders->save();

        //Kopelt de term aan een categorie in de pivot table
        return redirect()->route('zenders.show', $zenders->slug)->with('success', 'Zender aangepast');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return Response
     */
    public
    function destroy($id)
    {
        // Zet de slug variable als url
        $slug = strtolower($id);
        $zenders = Zenders::where('slug', '=', $slug)->get()->first();
        if (!$zenders) {
            abort(404);
        }

        $response = "Je hebt {$zenders->naam} verwijderd";
        $zenders->delete();

        return redirect()->route('zenders.index')->with('deleted', $response);
    }
}
