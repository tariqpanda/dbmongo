<?php

namespace App\Http\Controllers;

use App\Zenders;
use Illuminate\Http\Request;

class PageController extends Controller
{
    public function home()
    {
        $zenders = Zenders::all()->sortByDesc('created_at');
        return view('home', compact('zenders'));
    }
}
