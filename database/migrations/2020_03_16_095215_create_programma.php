<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProgramma extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('programma', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('programma_naam',45);
            $table->date('datum');
            $table->time('begin_tijd');
            $table->time('eind_tijd');
            $table->integer('duratie');
            $table->string('slug');
            $table->unsignedBigInteger('presentator_id');
            $table->foreign('presentator_id')->references('id')->on('presentator');
            $table->unsignedBigInteger('zenders_id');
            $table->foreign('zenders_id')->references('id')->on('zenders');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('programma');
    }
}
