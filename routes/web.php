<?php

Auth::routes([
    'register' => true
]);

Route::get('/', 'PageController@home');

Route::resource('zenders', 'ZenderController');
Route::resource('programma', 'ProgrammaController');
Route::get('zenders/search', 'ZenderController@search')->name('zenders.search');

Route::namespace('Admin')->prefix('admin')->name('admin.')->middleware('can:manage-users')->group(function(){
    Route::resource('/users', 'UsersController', ['except' =>['show', 'create', 'store']]);
});
